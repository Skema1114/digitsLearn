const express = require('express');
const routes = express.Router();
const neuralNetwork = require('./neuralNetwork');

routes.get("/", neuralNetwork.get);

module.exports = routes;