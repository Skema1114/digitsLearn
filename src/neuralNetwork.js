const synaptic = require('synaptic');
const express = require('express');
const mnist = require('mnist');

module.exports = {
  get(request, response) {
    const set = mnist.set(1000, 100);
    const trainingSet = set.training;
    const testSet = set.test;
    const Layer = synaptic.Layer;
    const Network = synaptic.Network;
    const Trainer = synaptic.Trainer;
    const inputLayer = new Layer(784);
    const hiddenLayer = new Layer(100);
    const outputLayer = new Layer(10);

    inputLayer.project(hiddenLayer);
    hiddenLayer.project(outputLayer);

    const myNetwork = new Network({
      input: inputLayer,
      hidden: [hiddenLayer],
      output: outputLayer
    });

    const trainer = new Trainer(myNetwork);
    trainer.train(trainingSet, {
      rate: .1,
      iterations: 100,
      error: .1,
      shuffle: true,
      log: 1,
      cost: Trainer.cost.CROSS_ENTROPY
    });

    console.log(myNetwork.activate(testSet[0].input));
    console.log(testSet[0].output);
    return res.json({ message: "Server funfando que é uma beleza" });
  }
}